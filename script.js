const productsURL = 'https://fakestoreapi.com/products'

fetch(productsURL)
    .then((response) => {
        if (response.ok){
            return response.json();
        }
        throw new Error(response.status)
    })
    .then((data) => {
        if(data.length === 0){
            errorHandler("no data");
            return;
        }
        loadToHtml(data);
    })
    .catch((err) => {
        console.log('error occurred', err);
        errorHandler(err);
    })


const allProducts = document.querySelector(".all-products");
const categoryDisplay = document.querySelector('.category-display');
const categoryDisplayState =categoryDisplay.innerHTML;
let productCards;

const errorHandler = (err) => {
    if(err instanceof Error){
        allProducts.innerHTML = `<h1 style="color:red;">Error occured while fetching data</h1>`;
    }
    else if(err === "no data"){
        allProducts.innerHTML = `<h1>No products to display, come back later</h1>`;
    }
}

allProducts.innerHTML =`<div class="loading"></div>`;

function goBack(){
    allProducts.innerHTML = "";
    let index=0;
    while(index<productCards.length){
        console.log(index);
        allProducts.innerHTML+=productCards[index];
        index+=1;
    }
    categoryDisplay.innerHTML= categoryDisplayState;
    loadToHtml();
}
const singleProductElement = (productData) => {
    allProducts.innerHTML = `<div class="single-product">
    <div class = "single-prod-image">
    <img src = "${productData.image}"></img>
    </div>
    <div class="single-prod-details">
        <h1>${productData.title}</h1>
        <p class="single-prod-rating"><span>&#9734</span>${productData.rating.rate}/5 <span>(${productData.rating.count})</span></p>   
        <p class="single-prod-price"><span>$</span> ${productData.price}</p>
        <p class="single-prod-description">${productData.description}</p>

    </div>
    </div>
    `;

}

const singleProductLoad = (prodId) => {
    allProducts.innerHTML =`<div class="loading"></div>`;
    categoryDisplay.innerHTML = `<button class="back" onclick="goBack()">back</button>`;

    const singleProductURL = `https://fakestoreapi.com/products/${prodId}`;
    fetch(singleProductURL)
        .then((response) => {
            if (response.ok){
                return response.json();
            }
            throw new Error(response.status);
        })
        .then((data) => {
            if(data.length === 0){
                errorHandler("no data");
                return;
            }
            console.log(data);
            singleProductElement(data);
        })
        .catch((err) => {
            console.log('error occurred', err);
            errorHandler(err);
        })
}

const loadToHtml = (data) => {
    productCards = data.map((product) => {
        allProducts.innerHTML="";
        return `<div class="product-card" id = "product-card" onclick="singleProductLoad(${product.id})">
        <div class = "prod-image">
            <img src="${product.image}">
        </div>
        <div class="info">
        <h4>${product.title}</h4>
        <p class="prod-rating"><span>&#9734</span>${product.rating.rate}/5 <span>(${product.rating.count})</span></p>   
        <p class="prod-price"><span>$</span> ${product.price}</p>
        <p class="prod-description">${product.description.slice(0, 100) + "..."}</p>
        </div>
    </div>`;
    })

    //adding content to all products div
    
    let index=0;
    while(index<productCards.length){
        console.log(index);
        allProducts.innerHTML+=productCards[index];
        index+=1;
    }
}
