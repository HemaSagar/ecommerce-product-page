const firstName = document.getElementById("first-name");
const lastName = document.getElementById("last-name");
const email = document.getElementById("email");
const password = document.getElementById("password");
const confirmPassword = document.getElementById('confirm-password');
const checkbox = document.getElementById("terms-conditions");
const form = document.getElementById("signup-form");
const validator = { "fname": false, "lname": false, "email": false, "password": false, "confirmPassword": false };

const nameValidOrNot = (name) => {
    const validRegex = new RegExp(/^[a-zA-Z]{1,}$/);
    console.log(validRegex.test(name));
    return validRegex.test(name);
}

const firstNameCheck = () => {
    const validateMessage = document.getElementById("first-name-checker");
    const name = firstName.value;

    if(nameValidOrNot(name)) {
        validateMessage.style.visibility = "hidden";
        firstName.style.border = "2px solid green";
        validator["fname"] = true;
    }
    else {
        validateMessage.innerText = "Fill this field with only alphabets and no spaces";
        validateMessage.style.visibility = "visible";
        validateMessage.style.color = "red";
        firstName.style.border = "2px solid red";
        validator["fname"] = false;
    }
}

const lastNameCheck = () => {
    const validateMessage = document.getElementById("last-name-checker");
    const name = lastName.value;
    if(nameValidOrNot(name)) {
        validateMessage.style.visibility = "hidden";
        lastName.style.border = "2px solid green";
        validator["lname"] = true;
    }
    else {
        validateMessage.innerText = "Fill this field with only alphabets and no spaces";
        validateMessage.style.visibility = "visible";
        validateMessage.style.color = "red";
        lastName.style.border = "2px solid red";
        validator["lname"] = false;
    }


}

const emailCheck = () => {
    const validateMessage = document.getElementById("email-checker");
    const validRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/);

  
    if (validRegex.test(email.value)) {
        validateMessage.style.visibility = "hidden";
        email.style.border = "2px solid green";
        validator["email"] = true;
    }
    else {
        validateMessage.innerText = "Enter valid email";
        validateMessage.style.visibility = "visible";
        validateMessage.style.color = "red";
        email.style.border = "2px solid red";
        validator["email"] = false;
    }

}


const passwordCheck = () => {
    const validateMessage = document.getElementById("password-checker");
    if (password.value.length < 8 || password.value.length > 20) {
        validateMessage.innerHTML = "Password must be 8-20 characters";
        validateMessage.style.visibility = "visible";
        validateMessage.style.color = "red";
        password.style.border = "2px solid red";
        validator["password"] = false;

    }
    else {
        validateMessage.style.visibility = "hidden";
        password.style.border = "2px solid green";
        validator["password"] = true;
    }
    confirmPasswordCheck();
}


const confirmPasswordCheck = () => {
    const validateMessage = document.getElementById("confirm-password-checker");
    if (confirmPassword.value === password.value && confirmPassword.value.length!==0) {
        validateMessage.style.visibility = "hidden";
        confirmPassword.style.border = "2px solid green";
        validator["confirmPassword"] = true;

    }
    else {
        validateMessage.innerHTML = "Password doesn't match";
        validateMessage.style.visibility = "visible";
        validateMessage.style.color = "red";
        confirmPassword.style.border = "2px solid red";
        validator["confirmPassword"] = false;

    }
}

const checkboxChecker = () => {
    const validateMessage = document.getElementById("checkbox-checker");
    if(!checkbox.checked){
        validateMessage.innerText="Agree to terms and conditions";
        validateMessage.style.visibility="visible";
        validateMessage.style.color = "red";
    }
    else{
        validateMessage.style.visibility="hidden";
    }
}




form.addEventListener('submit', (event) => {
    if (Object.values(validator).includes(false)) {
        event.preventDefault();
        firstNameCheck();
        lastNameCheck();
        emailCheck();
        passwordCheck();
        confirmPasswordCheck();
        checkboxChecker();
    }
    else {
        localStorage.setItem("firstname", document.getElementById("first-name").value);
        localStorage.setItem("lastname", document.getElementById("last-name").value);
        localStorage.setItem("email", document.getElementById("email").value);

        console.log(localStorage.getItem("firstname"));
        console.log(localStorage.getItem("lastname"));
        console.log(localStorage.getItem("email"));
    }
})

//show message if the user is already signedup


if (localStorage.getItem("firstname")) {
    console.log("welcome");
    const welcomeMessage = document.getElementById("create-account");
    // welcomeMessage.innerHTML=`<img src="./images/success.png" styles="widht=50% height=50%;object-fit=cover"></img>
    // <h1>Welcome</h1>`;
    welcomeMessage.innerHTML=`<p style="color:black;font-size:1.5rem;text-align:center">Welcome ${localStorage.getItem("firstname")} you are already signed up</p>
    <img src="./images/success.png" width="100px" height="100px" style="margin-left:auto;margin-right:auto;display:block">`;
    form.style.display="none";

    const signupBtn = document.getElementById("signup-link");
    signupBtn.innerText = "Sign Out";
    signupBtn.onclick=() => {
        
        localStorage.clear();
    }
    
}




